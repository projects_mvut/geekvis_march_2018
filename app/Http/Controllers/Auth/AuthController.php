<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Arrow;
use Validator;
use \Mail;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/
	protected $redirectPath = 'dashboard';
	protected $loginPath = 'login';
	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required',
			]);
	}

	public function authenticated(Request $request){
		if($request->session()->has('nextPost')){
			return redirect('/post/'.$request->session()->pull('nextPost'));
		}
		return redirect()->intended();
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(array $data)
	{
		if(env('ENABLE_RECAPTCHA')){
			$captcha=$data['g-recaptcha-response'];
			$respon=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".env('GOOGLE_RECAPTCHA_SECRET')."&response=".$captcha);
			$respon=json_decode($respon,true);
			if(!$respon['success']){
				return redirect()->back()->with('status-danger','Robot');
			}
		}
		$username=str_slug($data['name']).'-'.str_random(8);
		$user=User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'username' => $username,
			'password' => bcrypt($data['password']),
			'token'=>str_random(60)
			]);
		$himanshu=User::where('email','himanshu.vasistha@gmail.com')->first();
		if($himanshu){
			Arrow::create(['user_id'=>$user->id,'head_id'=>$himanshu->id]);
			Arrow::create(['head_id'=>$user->id,'user_id'=>$himanshu->id]);
		}
		Mail::queue('emails.verify', ['user' => $user], function($message) use ($user){
			$message->to($user->email, $user->username)
			->subject(env('SITE_NAME').' | Verify Your email');
		});
		Mail::queue('emails.notify',['user'=>$user],
			function($m){
				$m->to('sonali.vijan@gmail.com','Sonali')
				->cc('himanshu.vasistha@gmail.com','Himanshu')
				->subject(env('SITE_NAME').' User Register Notification');
			});
		return $user;
	}
}
