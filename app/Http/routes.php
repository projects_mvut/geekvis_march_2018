<?php

if(env('HTTPS')){
	URL::forceSchema('https');
}
// Route::get('uploaddata', 'PageController@uploadData');
// Route::post('uploaddata', 'PageController@saveData');

Route::get('json/users', "PageController@userJson");
Route::get('profile/{user}/unstar', "CommentController@unstar");
Route::get('rss', "PostController@rss");
Route::get('search', "PageController@search");
Route::get('about-us', "PageController@about");
Route::get('privacy', "PageController@privacy");
Route::get('contact', "PageController@contact");
Route::get('notifications', "NotificationController@index");

Route::get('login/next/{post}', 'PageController@next');
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@postRegister');
Route::get('login/{social}', 'SocialController@redirectToSocial');
Route::get('facebook', 'SocialController@handleSocialCallback');
Route::get('google', 'SocialController@handleSocialCallback');

Route::get('verify', 'PageController@verify');
Route::post('verify', 'PageController@sendToken');
Route::get('verify/{token}', 'PageController@verifyToken');

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('dashboard',"DashboardController@index");

Route::get('dashboard/ads','DashboardController@ads');
Route::post('dashboard/ad1','DashboardController@save_ad1');
Route::post('dashboard/ad2','DashboardController@save_ad2');

Route::get('dashboard/email','DashboardController@email');
Route::post('dashboard/email','DashboardController@send_email');

Route::get('dashboard/images','ImageController@index');
Route::post('dashboard/images','ImageController@store');
Route::get('dashboard/images/{id}/delete','ImageController@delete');

Route::get('dashboard/admins',"DashboardController@admins");
Route::get('dashboard/editors',"DashboardController@editors");
Route::get('dashboard/users',"DashboardController@users");
Route::get('dashboard/{user}/delete',"DashboardController@user_delete");
Route::get('dashboard/{user}/makeEditor',"DashboardController@makeEditor");
Route::get('dashboard/{user}/makeUser',"DashboardController@makeUser");

Route::get('dashboard/messages',"DashboardController@messages");
Route::get('dashboard/message/{message}/delete',"DashboardController@delete_message");

Route::get('dashboard/posts',"DashboardController@posts");
Route::get('dashboard/{user}/posts',"DashboardController@user_posts");

Route::get('dashboard/user/create',"ProfileController@create");
Route::post('dashboard/user',"ProfileController@store");
Route::get('dashboard/{user}/profile',"ProfileController@show");
Route::patch('dashboard/{user}','ProfileController@update');
Route::patch('dashboard/{user}/password','ProfileController@password');

Route::get('profile/{user}',"ProfileController@wall");
Route::get('/', 'PageController@index');

Route::get('follow/{user}', 'ArrowController@follow');
Route::get('unfollow/{user}', 'ArrowController@unfollow');

Route::get('posts','PostController@index');
Route::get('post/create','PostController@create');
Route::get('post/{post}','PostController@show');
Route::get('embed/{post}','PostController@embed');
Route::post('post','PostController@store');
Route::get('post/{post}/edit','PostController@edit');
Route::patch('post/{post}','PostController@update');
Route::get('post/{post}/delete','PostController@delete');

Route::get('post/{post}/boost','PostController@boost');
Route::get('post/{post}/unboost','PostController@unboost');
Route::get('post/{post}/like','CommentController@like');
Route::get('post/{post}/unlike','CommentController@unlike');
Route::post('post/{post}/comment','CommentController@save');
Route::get('post/{post}/comment/{comment}/delete','CommentController@delete');
Route::get('comment/{comment}/vote','CommentController@vote');
Route::get('comment/{comment}/unvote','CommentController@unvote');


Route::get('category/{category}','CategoryController@index');
Route::post('category','CategoryController@save');
Route::get('category/{category}/edit','CategoryController@edit');
Route::patch('category/{category}','CategoryController@update');
Route::get('category/{category}/delete','CategoryController@delete');

Route::post('message','PageController@sendMessage');
Route::get('dashboard/properify','DashboardController@properify');