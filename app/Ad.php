<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
	public $timestamps = false;
	protected $fillable=['url','imgUrl','title'];
}
