<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Auth;

class StarEvent extends Event implements ShouldBroadcast
{
	use SerializesModels;

	public $data;

	public function __construct($username)
	{
		$data=[];
		$data['user']=$username;
		$data['star']=1;
		$this->data = $data;
	}

	public function broadcastOn()
	{
		return ['star-channel'];
	}
}
