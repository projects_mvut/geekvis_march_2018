@extends('layouts.dashboard')

@section('dashboardcontent')

<div class=" row ad">
	<div class="col col-sm-6">		
		<h1>Modify List Ad</h1>
		{!!Form::open(['url'=>'dashboard/ad1','files' => true])!!}
		<div class="form-group">
			{!! Form::text('title',null,['placeholder'=>'Title','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::text('url',null,['placeholder'=>'Url','class'=>'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('Image') !!}
			{!! Form::file('imgUrl', null,['class'=>'btn btn-primary']) !!}
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-primary form-control">Update</button>
		</div>
		{!! Form::close() !!}
		<div class="adss">
			@if($ad)
			<a href="{{$ad->url}}"><img src="{{$ad->imgUrl}}" alt="{{$ad->title}}"></a>
			@endif
		</div>
	</div>
	<div class="col col-sm-6">
		<h1>Modify Post Ad</h1>
		{!!Form::open(['url'=>'dashboard/ad2','files' => true])!!}
		<div class="form-group">
			{!! Form::text('title',null,['placeholder'=>'Title','class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::text('url',null,['placeholder'=>'Url','class'=>'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('Image') !!}
			{!! Form::file('imgUrl', null,['class'=>'btn btn-primary']) !!}
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-primary form-control">Update</button>
		</div>
		{!! Form::close() !!}
		<div class="adss">
			@if($ad)
			<a href="{{$ad2->url}}"><img src="{{$ad2->imgUrl}}" alt="{{$ad2->title}}"></a>
			@endif
		</div>
	</div>
	@stop