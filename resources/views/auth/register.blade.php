@extends('layouts.page')
@section('pageTitle', 'Register for a GeekVis account')
@section('pagecontent')



<section class="authorization">
<div class="col-md-10 col-md-offset-1 col-xs-12">
<div class="row equal" >
<div class="col-md-4 hidden-xs left">
<div class="inner">
<img src="/images/favicon.png" class="img-responsive"  id="owl">
    <h3 class="text-center">Sign Up</h3>
</div>
</div>
<div class="col-md-8 col-xs-12 right">
<div class="inner line_c">
<div class="auth">
 <form method="POST" action="/register">
    {!! csrf_field() !!}

    <div class="form-group">        
        <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Name">
    </div>

    <div class="form-group">        
        <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email">
    </div>

    <div class="form-group">        
        <input type="password" name="password" class="form-control" placeholder="Password">
    </div>
        <div class="form-group">        

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}"></div>
</div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary form-control">Sign up</button>
    </div>
</form>
<hr>
<div class="text-center">Or Sign up with</div>
<hr>
<a href="/login/facebook" class="btn btn-social btn-f"><i class="fab fa-facebook-f"></i> Facebook</a>
<a href="/login/google" class="btn btn-social btn-g"> <i class="fab fa-google-plus-g"></i> Google</a>
</div>
</div>

</div>

</div>
</div>
</div>
</section>
@stop
