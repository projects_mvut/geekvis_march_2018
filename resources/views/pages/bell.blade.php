@extends ('layouts.page')

@section('pagecontent')
<div id="notifications">

<h1>Notifications</h1>


@if(!$notifications->isEmpty())
<div class="bell">
	@foreach($notifications as $notification)
	<span class="ring">
		<a href="/profile/{{$notification->user->username}}">{{$notification->user->name}}</a>
		@if($notification->type)
		Commented On
		@else
		Liked
		@endif
		<a href="/post/{{$notification->post->slug}}">{{$notification->post->title}}</a>
	</span>
	@endforeach
</div>
@else
<p>No Notifications found.</p>
@endif

</div>
@stop