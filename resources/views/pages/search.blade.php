@extends ('layouts.pageFluid')

@section('headinclude')
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
@stop

@section('pagecontent')
<h1>Search Results for <span style="text-decoration:underline;">{{$query}}</span></h1>
<br>
<div id="wall">
	<div class="">
		<div class="col col-sm-4 col-sm-push-8">
			@include('partials.ad')
			@include('partials.ad')
			@include('partials.ad')
		</div>
		<div class="col col-sm-8 col-sm-pull-4 wall posts">
			<div class="row">
				@if(!$articles->isEmpty())
				@foreach($articles as $key=>$post)
				@if($key==1)
				<div class="col col-sm-12 publish-ads bottom-margin" style="background-color: #fff">
					<div class="row">
						<div class="ad">
							<div class="html-ads-home">
								<a href="https://mindvis.in/courses/gate-2018-mechanical-engineering-online-course" target="_blank">
									<!--<img src="/images/Online-Education.jpg">-->
									<div class="ads-content">
										<h1>GATE <span> mechanical</span>2018 <span>online coaching</span></h1>
										<p>visit </p>
										<hr></hr>
										<h5>www.mindvis.in</h5>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
				@endif
				@if($key==2)
				<div class="col col-sm-12 publish-ads bottom-margin" style="background-color: #fff">
					<div class="row">
						<a href="https://www.facebook.com/groups/Chandigarh.Helpost/" target="_blank">
							<div class="publish-ads-top">
								<img src="/images/publish-img.jpg">
								<p> Chandigarh Helpost is a community of over 1 million people all over the world among 123 countries. They have a spirit that is not bound by boundaries, castes, colors, classes, names, sexes, professions, genres because this is the spirit of helping, this spirit is present everywhere, omnipresent which they call it the spirit of 'OWON, one world one nation</p>
							</div>
							<div class="publish-lower"><img src="/images/helpostlogopost.jpg">
								<p>An active community for Business/Markit/Customers 50,000 Daily Postings</p>
								<h3>Get #FEATURED</h3>
								<p>Write to us at ADVERTISE@HELPOSTCOM</p>
							</div>
						</a>
					</div>
				</div>
				@endif
				@if($key==3)
				@include('partials.ad')
				@endif
				@include('partials.elements.postbox')
				@endforeach
				{!! $articles->appends($_GET)->render() !!}

			</div>
			@else
			<p>No Posts found.</p>
			@endif
		</div>
		
	</div>	
</div>
@stop