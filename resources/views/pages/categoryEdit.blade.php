@extends('layouts.page')

@section('pagecontent')

{!!Form::model($category,['url'=>'category/'.$category->slug,'method'=>'patch'])!!}
<div class="form-group">
	{!! Form::label('Edit Category') !!}
	{!! Form::text('title',null,['placeholder'=>'Title','class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::submit('Submit',['class'=>'btn btn-primary','name'=>"submit"]) !!}
</div>
{!!Form::close()!!}
@stop