@extends('main')

@section('maincontent')
    <div class="container-fluid">
    <div class="row">
      @yield('pagecontent')
      </div>
    </div>
@stop