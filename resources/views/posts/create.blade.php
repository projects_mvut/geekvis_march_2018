<div id="createPostModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Create Post</h4>
			</div>
			<div class="modal-body">
				{!!Form::open(['url'=>'post','files'=>true,'class'=>'form-horizontal'])!!}
				@include('partials.forms.post')
			</div>
		</div>

	</div>
</div>