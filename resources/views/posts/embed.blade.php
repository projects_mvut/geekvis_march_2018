@extends('embed')
@section('maincontent')
<div id="post" class="postbox">
	<span class="title"> <img src="/images/{{$post->type}}.png" alt=""> {{$post->title}}</span>
	@if($post->category)
	<a href="/category/{{$post->category->slug}}" class="cat">
		<a class="cat" href="/post/{{$post->slug}}"> <i class="fa fa-bars"></i>
			{{$post->category->title}}
		</a>
	</a>
	@else
	<a href="#" class="cat">
		<span class="cat"> <i class="fa fa-bars"></i>
		</span>
	</a>
	@endif
	<div class="date">
		<i class="fa fa-clock-o"></i> Shared {{$post->created_at->diffForHumans()}}
		<span class="views"> <i class="fa fa-eye"></i> {{$post->views}} Views</span>
	</div>
	<a href="/profile/{{$post->user->username}}" class="user">
		<img src="{{$post->user->imgUrl}}" alt="">
		<span class="name">{{$post->user->name}}</span>
		<span class="bio">{{$post->user->bio}}</span>
	</a>

	<img src="{{$post->imgUrl}}" class="postimg">
	<div class="content">{!! $post->content !!}</div>
	<div class="intract">
		<span class="sat"><i class="fa fa-comments-o"></i> {{$post->comments->count()}} </span>
	</div>
</div>
@stop