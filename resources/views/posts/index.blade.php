@extends('layouts.pageFluid')

@section('headinclude')
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

@if(Auth::check())
@include('partials.forms.tinymce')
@endif
@stop
@section('pagecontent')
<section style="background:#fff" class="heading-categ">
	<div class="container-fluid">
		<div class="row">
			<div class="outer-wrapper" style="padding:15px 0;">
				<div class="col-md-12">
					<div class="heading-bar">
						<h3>Select Your Interest</h3>
					</div>
				</div>

@foreach($cats as $cat)


<div class="col-xs-12 col-md-4 col-lg-4 text-center customclass customclass2">
  <span style="width: 50px;
    height: 50px;    text-align: center;
    -webkit-border-radius: 70px;
    -moz-border-radius: 70px;
    border-radius: 70px;">           <i class="fas fa-angle-right" style="color:#fe8b6c;"></i>
</span> 

           <a href="/category/{{$cat->slug}}" style="">
{{$cat->title}}
</a>
@if(Auth::check())
@if(Auth::user()->admin)



<div class="btn-group" style="    position: absolute;
    right: 5px;">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fas fa-caret-square-down"></i>


  </button>
  <div class="dropdown-menu">
  <a href="/category/{{$cat->slug}}/edit">
<i class="fas fa-pencil-alt"></i>
</a>
	<a href="/category/{{$cat->slug}}/delete">
<i class="fas fa-trash-alt"></i>

</a>
  </div>
</div>
						
								@endif
								@endif
        </div>

								
							@endforeach
</div>
</div>
</div>
</section>

<div id="wall">
	<div class="">
		<div class="col-sm-12 text-center" style="margin:15px 0;">
			<div class="row">
				<div class="col-md-12">
				<div class="row">
					<div class="top-bar">
					<div class="col-md-4 col-xs-12">
					<div class="text-center">

						@if(Auth::check())
						<button type="button" class="btnCreatePost" data-toggle="modal" data-target="#createPostModal" style="width: 100%">
							<i class="fa fa-plus"></i> New Post
						</button>
						@include('posts.create')
						@else
						@endif
						</div>
						</div>
						<div class="spacer visible-xs"></div>
						<div class="col-md-4 col-xs-12">
						<div class="text-center">
						<a href="#" class="btnCreatePost" data-toggle="modal"
data-target="#fsModal" style="width: 100%"><i class="fa fa-plus"></i> Select Category</a>
						</div>
						</div>
						
							<div class="spacer visible-xs"></div>
							<div class="col-md-4 col-xs-12">
							<div class="text-center">
							@if(Auth::check())
							@if(Auth::user()->admin)
							<a href="#" class="btnCreatePost" data-toggle="modal" data-target="#smallModal" style="width: 100%"><i class="fa fa-plus"></i> Add New Category</a>



							<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
								<div class="modal-dialog modal-sm">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Add New Category</h4>
										</div>
										<div class="modal-body">


											<div class="createCate">
												{!!Form::open(['url'=>'category'])!!}
												<div class="form-group">
													{!! Form::text('title',null,['placeholder'=>'Add Category','class'=>'form-control']) !!}
												</div>




											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												{!! Form::submit('Submit',['class'=>'btn btn-primary','name'=>"submit"]) !!}
												{!!Form::close()!!}
											</div>
										</div>
									</div>
								</div>
							</div>





							@endif
							@endif

</div>
</div>


</div> <!-- added here -->



						</div>

					</div>
				</div>
			</div>



			

			
			<div class="col col-sm-3 col-sm-push-9">
				@foreach($boosted as $boost)
				<div class="boosted">
					@if($boost->boostUrl)
					<a href="/post/{{$boost->slug}}"><img src="{{$boost->boostUrl}}" alt=""></a>
					@elseif($boost->imgUrl)
					<a href="/post/{{$boost->slug}}"><img src="{{$boost->imgUrl}}" alt=""></a>
					@else
					<a href="/post/{{$boost->slug}}"><h4>{{$boost->title}}</h4></a>
					@endif
				</div>
				@endforeach
				@include('partials.ad')
				<div class="spacer" style="line-height:18px">&nbsp;</div>

				@include('partials.ad')
                          <div class="spacer" style="line-height:18px">&nbsp;</div>

				@include('partials.ad')
				<div class="spacer" style="line-height:18px">&nbsp;</div>

		</div>
			<div class="col col-sm-9 col-sm-pull-3 col-xs-12 wall posts">
				<div class="row equal">
<?php $i=0; ?>
					@foreach($posts as $key=>$post)
					@if($i%3==0)
			<div id="post" class="postbox bottom-margin col-md-4 col-sm-4 col-lg-4" style="padding: 0px 0; padding-bottom: 0">
			@else
			<div id="post" class="postbox bottom-margin col-md-8 col-sm-8 col-lg-8" style="padding: 0px 0; padding-bottom: 0">
			@endif
			@include('partials.elements.postbox')
			<?php $i++; ?>
					@endforeach



				</div>
								<div style="width:100%" class="text-center"> 	
								{!! $posts->render() !!}
								</div>

			</div>
		</div>	
	</div>








<!-- view modal -->

<!-- modal -->
<div id="fsModal"
     class="modal animated bounceIn"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">

  <!-- dialog -->
  <div class="modal-dialog">

    <!-- content -->
    <div class="modal-content">

      <!-- header -->
      <div class="modal-header">
        <h1 id="myModalLabel"
            class="modal-title text-center">
          Select Your Interest
        </h1>
      </div>
      <!-- header -->
      
      <!-- body -->
      <div class="modal-body">
<div class="row">
        
        


@foreach($cats as $cat)


<div class="col-xs-12 col-md-4 col-lg-4 text-center customclass">
            <span style="width: 50px;
    height: 50px;    text-align: center;
    -webkit-border-radius: 70px;
    -moz-border-radius: 70px;
    border-radius: 70px;">           <i class="fas fa-angle-right" style="color:#fe8b6c;"></i>
</span> 
           <a href="/category/{{$cat->slug}}" style="">
{{$cat->title}}
</a>
@if(Auth::check())
@if(Auth::user()->admin)
<div class="btn-group" style="    position: absolute;
    right: 5px;">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fas fa-caret-square-down"></i>


  </button>
  <div class="dropdown-menu">
  <a href="/category/{{$cat->slug}}/edit">
<i class="fas fa-pencil-alt"></i>
</a>
	<a href="/category/{{$cat->slug}}/delete">
<i class="fas fa-trash-alt"></i>

</a>
  </div>
</div>					
								@endif
								@endif
        </div>

								
							@endforeach








       
    </div>
      
      </div>
      <!-- body -->

      <!-- footer -->
      <div class="modal-footer hidden">
        <button class="btn btn-secondary"
                data-dismiss="modal">
          close
        </button>
        <button class="btn btn-default">
          Default
        </button>
        <button class="btn btn-primary">
          Primary
        </button>
      </div>
      <!-- footer -->

    </div>
    <!-- content -->

  </div>
  <!-- dialog -->

</div>
<!-- modal -->



<script>
function newF(m){
var n=document.getElementById(m).value;
var lastPart = n.split("/").pop();
if(lastPart == 'delete'){
var confirmVal = confirm("Do you want to delete it?");
      if( confirmVal == true ){
          location=n;  
          return true;
      }
else{
return false;
}
}
else{
location=n;
}
}
</script>

@if(Auth::check() && Request::path() == '/')
<script type="text/javascript">
$("nav").css({"height": "50px", "margin-top": "0", "background": "#fff","box-shadow":"0 0 10px rgba(0,0,0,.1)"});
$("nav").removeClass('nav-up');
$("nav .container").css({"box-shadow": "0px 0px 0px #000"});
$("body").css({"margin-top": "40px"});
</script>
@endif
	@stop
