
<div class="col-sm-12">
	<div class="row">
					<!-- <h4 style="padding: 10px; background-color: 555">Comments</h4> -->
		<div class="comments">

			@foreach($comments as $comment)





			<div class="col-sm-12 comment-box">
			<div class="row">
			<div class="panel panel-white post panel-shadow commenting">
			
			<a href="/profile/{{$comment->user->username}}" class="user">
					<div class="post-heading">
						<div class="pull-left image">
							<img src="{{$comment->user->imgUrl}}" class="img-circle avatar" alt="user profile image">
						</div>
						<div class="pull-left meta">
							<div class="h4">
								<a href="/profile/{{$comment->user->username}}"><b>{{$comment->user->name}}</b></a>
							</div>
							<h6 class="text-muted time">{{$comment->user->bio}}</h6>
						</div>
					</div> 
					</a>
					<div class="post-description"> 
					
					<div class="commentImg">
										<img src="{{$comment->imgUrl}}" class="postimg postModalImage img-responsive" data-post="{{$comment->id}}">
										<div class="modalQuestion modalQuestionHolder-{{$comment->id}}">
											<img src="{{$comment->imgUrl}}" alt="" class="img-responsive">
										</div>
									</div>
						<p>		{!! $comment->content !!}</p>
						<div class="stats">
							@if(Auth::check())
									@if(\App\Vote::where('user_id',Auth::user()->id)->where('comment_id',$comment->id)->count())
									<span class="liked"> Liked</span>
									<a href="/comment/{{$comment->id}}/unvote" class="unlike"></i> Unlike</a>
									@else
									<a href="/comment/{{$comment->id}}/vote" class="like"> Like</a>
									@endif
									@endif
									<span class="sat"> {{$comment->votes->count()}} </span>
									@if(Auth::check())
												@if((Auth::user()->admin) or ((Auth::user()->id) == ($comment->user->id)))
												<a href="/post/{{$post->slug}}/comment/{{$comment->id}}/delete" class="cross pull-right"><i class="fa fa-times"></i></a>
												@endif
												@endif

						</div>
					</div>
				</div>
			</div>
			</div>






			

@endforeach
</div>
<!-- status elements -->
</div>



