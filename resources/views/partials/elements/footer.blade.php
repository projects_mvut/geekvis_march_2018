<footer>
	<div class="container">
		<div class="">
			<div class="outer-wrapper" style="padding: 5px 0px;">
				
				<div class="col-md-3 col-sm-3 col-xs-6">
					<div class="heading-small">
						<h4>Information</h4>
					</div>
					<div class="col-md-6">
					<div class="row">
					<ul id="vertical-menu">
						<li><a href="/about-us">About Us</a></li>
						<li><a href="/privacy">Privacy Policy</a></li>
					</ul>
					</div>
					</div>
					<div class="col-md-6">
					<div class="row">
					<ul id="vertical-menu">
						<li><a href="">Terms of Use</a></li>
						<li><a href="/contact">Contact Us</a></li>
					</ul>
					</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<div class="heading-small">
						<h4>Follow Us</h4>
					</div>
					
					<address>
					GeekVis<br>

					F-452, Industrial Area, Phase 8 B,<br>

					Mohali - 160055, Punjab, India.<br>

					Phone: +91-8283865488

					</address>
					
					

				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<div class="heading-small">
						<h4>Connect with us</h4>
					</div>
					<a style="color: #fe8b6c;" href="mailto:info@gmail.com"><i class="fa fa-envelope"></i>&nbsp;info@geekvis.com</a><br><br>
					<a href="https://www.facebook.com/geekvis/" target="_blank"><i class="fab fa-facebook fa-3x" style="color:#3b5998"></i></a>&nbsp;<a href="https://twitter.com/geekvis_india" target="_blank"><i class="fab fa-twitter-square fa-3x" style="color:#0084b4"></i></a>

				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<img src="/images/head.png" width="150" style="margin-bottom:10px;margin-top:16px;">
					<p>
						Join the learning revolution. Connect with best mentors for FREE for your exam preparation.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid" style="background:#ff6745; color: #fff;">
		<div class="row">
			<div class="outer-wrapper text-center" style="padding:10px 0px;">
				&copy; Copyright 2017-GeekVis
			</div>
		</div>
	</div>
</footer>